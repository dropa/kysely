/**
 * Created by dropa on 26.8.2015.
 */

function secureTrueFunc(userId) {
    if (userId && Roles.userIsInRole(userId,'moderator')) {
        return true;
    }
    return false;
}
function trueFunc() {
    return true;}
function falseFunc() {
    return false;}

function requireAdmin(userId){
    return Roles.userIsInRole(userId,'admin');
}
function requireModerator(userId){
    return Roles.userIsInRole(userId,'moderator');
}
function requireReader(userId){
    return Roles.userIsInRole(userId,'reader');
}

function testiFunc(userId){
    return false;
}

/* N�m� s��nn�t ei p�de serversidella eik� methodeissa */

Images.allow({
    insert: secureTrueFunc,
    update: secureTrueFunc,
    remove: secureTrueFunc,
    download: trueFunc
});
Inquiries.allow({
    insert: requireModerator,
    update: requireModerator,
    remove: requireModerator
});
Answers.allow({
    insert: requireReader,
    update: falseFunc,
    remove: requireModerator
});
Contacts.allow({
    insert: requireReader,
    update: requireAdmin,
    remove: requireAdmin
});
Meteor.users.deny({
    insert: requireAdmin,
    update: requireAdmin,
    remove: requireAdmin
});



