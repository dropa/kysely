/**
 * Created by dropa on 26.8.2015.
 */

Meteor.publish("inquiries", function () {
    if(Roles.userIsInRole(this.userId, 'reader')){
        return Inquiries.find();
    }
    else{
        return false;
    }
});
Meteor.publish(null, function () {
    if (Roles.userIsInRole(this.userId, 'admin')) {
        return Meteor.users.find({},{fields:{services:0}});
    } else {
        this.stop();
    }
});
Meteor.publish(null, function() {
    if (this.userId) {
        return Answers.find();
    }
    else {
        return false;
    }
});
Meteor.publish(null, function() {
    if(this.userId){
        return Contacts.find();
    }
    else {
        return false;
    }
});
Meteor.publish("images", function(){
    return Images.find();
});
