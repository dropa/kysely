/**
 * Created by dropa on 26.8.2015.
 */

Meteor.startup(function () {

    if(!Meteor.users.find().count()) {
        console.log("-------------------\nNo users detected!\nCreating new admin\nUsername: admin\nPassword: admin\n-------------------");
        var options = {
            username: 'admin',
            password: 'admin'
        };
        var newAdminId = Accounts.createUser(options);
        Roles.addUsersToRoles(newAdminId, ['admin','moderator','reader']);
    }
    if(!Inquiries.find().count()){
        var test = {
            "_id": "3oCM6J9Nm8LNCnfHw",
            "topic": "Turhanpaivanen kysely",
            "createdAt": "2015-07-17T10:32:48.222Z",
            "createdBy": "admin",
            "lastAction": "Kysely uudelleennimetty",
            "active": false,
            "photos": [],
            "lastModifiedAt": "2015-07-20T09:48:43.090Z",
            "lastModifiedBy": "admin",
            "questions": [
                {
                    "question_id": "25516a5398a2be3d920cd360",
                    "questionType": "radio",
                    "questionText": "Kysymys ykkonen",
                    "options": [
                        {
                            "answer_id": "e700f37924f40ea79c7494d0",
                            "answerText": "ykkosen ykkonen"
                        },
                        {
                            "answer_id": "61e6b84afc462ee871b9c647",
                            "answerText": "ykkosen kakkonen"
                        },
                        {
                            "answer_id": "0e530b52ad0e2036aa78437f",
                            "answerText": "ykkosen kolmonen"
                        },
                        {
                            "answer_id": "d9df26877bc503d47f4947a6",
                            "answerText": "ykkosen nelonen"
                        },
                        {
                            "answer_id": "771f162153294d0688f83c64",
                            "answerText": "ykkosen vitonen"
                        }
                    ]
                },
                {
                    "question_id": "5291688c1ddd89b21ff8b610",
                    "questionType": "checkbox",
                    "questionText": "Kysymys kakkonen",
                    "options": [
                        {
                            "answer_id": "e65b9230480f23001af3196b",
                            "answerText": "kakkosen ykkonen"
                        },
                        {
                            "answer_id": "39f6fbebe12ae7734582252e",
                            "answerText": "kakkosen kakonen"
                        },
                        {
                            "answer_id": "33359c6387984ebe487cac8e",
                            "answerText": "kakkosen kolmonen"
                        },
                        {
                            "answer_id": "bca87c93d879c6fce72fe24f",
                            "answerText": "kakkosen nelonen"
                        },
                        {
                            "answer_id": "5f9064a1de91c47d8a0162f5",
                            "answerText": "kakkosen vitonen"
                        }
                    ]
                },
                {
                    "question_id": "312d03726673d3841c2565a5",
                    "questionType": "input",
                    "questionText": "Kysymys kolmonen"
                },
                {
                    "question_id": "efd03dfb18d8c50311741ebc",
                    "questionType": "radio",
                    "questionText": "Kysymys nelonen",
                    "options": [
                        {
                            "answer_id": "0030d494336ab82ee3a6ac99",
                            "answerText": "nelosen ykkonen"
                        },
                        {
                            "answer_id": "ac22cba651703d8e45bc1fd5",
                            "answerText": "nelosen kakkonen"
                        },
                        {
                            "answer_id": "923046c8827abeddb1e129da",
                            "answerText": "nelosen kolmonen"
                        },
                        {
                            "answer_id": "7f02b4699bb3a6fb9305bd46",
                            "answerText": "nelosen nelonen"
                        },
                        {
                            "answer_id": "3b776e867dd42cb4fbf67196",
                            "answerText": "nelosen vitonen"
                        }
                    ]
                },
                {
                    "question_id": "c6c698a2fa543d8ac83bf335",
                    "questionType": "checkbox",
                    "questionText": "Kysymys vitonen",
                    "options": [
                        {
                            "answer_id": "aeb637e4aebff88b3d2615c9",
                            "answerText": "vitosen ykkonen"
                        },
                        {
                            "answer_id": "2528c80ed61c7d73f6c5dab1",
                            "answerText": "vitosen kakkonen"
                        },
                        {
                            "answer_id": "8dac4e7d72292965a81551d1",
                            "answerText": "vitosen kolmonen"
                        },
                        {
                            "answer_id": "d4b8cece80a2de58ddb53576",
                            "answerText": "vitosen nelonen"
                        },
                        {
                            "answer_id": "21e4cfe1fd4893b4aa7e1ad1",
                            "answerText": "vitosen vitonen"
                        }
                    ]
                },
                {
                    "question_id": "06cbbd66c8310e57296b9f69",
                    "questionType": "input",
                    "questionText": "Kysymys kutonen"
                },
                {
                    "question_id": "f44718c229f5eea3f0b11ab4",
                    "questionType": "radio",
                    "questionText": "Kysymys seiska",
                    "options": [
                        {
                            "answer_id": "cf0f9a39c1951decb2fc3583",
                            "answerText": "seiskan ykkonen"
                        },
                        {
                            "answer_id": "8ead3f989318e03e32b0165a",
                            "answerText": "seiskan kakkonen"
                        },
                        {
                            "answer_id": "7228a6ad032479f15e652af3",
                            "answerText": "seiskan kolmonen"
                        },
                        {
                            "answer_id": "8bbc4c58d6ab480df1c0a40b",
                            "answerText": "seiskan nelonen"
                        },
                        {
                            "answer_id": "633af5c97b9193afa6db9e82",
                            "answerText": "seiskan vitonen"
                        }
                    ]
                },
                {
                    "question_id": "60982d2736db214b9a0d87c8",
                    "questionType": "checkbox",
                    "questionText": "Kysymys kasi",
                    "options": [
                        {
                            "answer_id": "afd48455ed4e586a3b2bed89",
                            "answerText": "kasin ykkonen"
                        },
                        {
                            "answer_id": "4211c932d613fe50482d22ec",
                            "answerText": "kasin kakkonen"
                        },
                        {
                            "answer_id": "209e819c6a5b98e6fc3af6b4",
                            "answerText": "kasin kolmonen"
                        },
                        {
                            "answer_id": "029e0d4960392756ce4fa5f0",
                            "answerText": "kasin nelonen"
                        },
                        {
                            "answer_id": "146466afe03a5b1d7e4a4d91",
                            "answerText": "kasin vitonen"
                        }
                    ]
                },
                {
                    "question_id": "53c462bb6dd45962c3c44c36",
                    "questionType": "input",
                    "questionText": "Kysymys ysi"
                }
            ]
        };
        var yyy = {
            "reference_id": "3oCM6J9Nm8LNCnfHw",
            "answers": {
                "25516a5398a2be3d920cd360": "0e530b52ad0e2036aa78437f",
                "5291688c1ddd89b21ff8b610": {
                    "e65b9230480f23001af3196b": false,
                    "39f6fbebe12ae7734582252e": true,
                    "33359c6387984ebe487cac8e": true,
                    "bca87c93d879c6fce72fe24f": true,
                    "5f9064a1de91c47d8a0162f5": false
                },
                "312d03726673d3841c2565a5": "dddd",
                "efd03dfb18d8c50311741ebc": "3b776e867dd42cb4fbf67196",
                "c6c698a2fa543d8ac83bf335": {
                    "aeb637e4aebff88b3d2615c9": false,
                    "2528c80ed61c7d73f6c5dab1": true,
                    "8dac4e7d72292965a81551d1": false,
                    "d4b8cece80a2de58ddb53576": true,
                    "21e4cfe1fd4893b4aa7e1ad1": true
                },
                "06cbbd66c8310e57296b9f69": "dsadsa",
                "f44718c229f5eea3f0b11ab4": "7228a6ad032479f15e652af3",
                "60982d2736db214b9a0d87c8": {
                    "afd48455ed4e586a3b2bed89": true,
                    "4211c932d613fe50482d22ec": true,
                    "209e819c6a5b98e6fc3af6b4": false,
                    "029e0d4960392756ce4fa5f0": true,
                    "146466afe03a5b1d7e4a4d91": false
                },
                "53c462bb6dd45962c3c44c36": "dsa"
            },
            "sent": "2015-07-22T11:38:20.622Z"
        };
        var kaa = {
            "reference_id": "3oCM6J9Nm8LNCnfHw",
            "answers": {
                "25516a5398a2be3d920cd360": "e700f37924f40ea79c7494d0",
                "5291688c1ddd89b21ff8b610": {
                    "e65b9230480f23001af3196b": false,
                    "39f6fbebe12ae7734582252e": false,
                    "33359c6387984ebe487cac8e": true,
                    "bca87c93d879c6fce72fe24f": true,
                    "5f9064a1de91c47d8a0162f5": true
                },
                "312d03726673d3841c2565a5": "d",
                "efd03dfb18d8c50311741ebc": "ac22cba651703d8e45bc1fd5",
                "c6c698a2fa543d8ac83bf335": {
                    "aeb637e4aebff88b3d2615c9": true,
                    "2528c80ed61c7d73f6c5dab1": true,
                    "8dac4e7d72292965a81551d1": true,
                    "d4b8cece80a2de58ddb53576": true,
                    "21e4cfe1fd4893b4aa7e1ad1": true
                },
                "06cbbd66c8310e57296b9f69": "sf ddasogli < a",
                "f44718c229f5eea3f0b11ab4": "8bbc4c58d6ab480df1c0a40b",
                "60982d2736db214b9a0d87c8": {
                    "afd48455ed4e586a3b2bed89": true,
                    "4211c932d613fe50482d22ec": true,
                    "209e819c6a5b98e6fc3af6b4": true,
                    "029e0d4960392756ce4fa5f0": true,
                    "146466afe03a5b1d7e4a4d91": true
                },
                "53c462bb6dd45962c3c44c36": "dddd"
            },
            "sent": "2015-07-22T11:39:10.474Z"
        };
        var koo = {
            "reference_id": "3oCM6J9Nm8LNCnfHw",
            "answers": {
                "25516a5398a2be3d920cd360": "61e6b84afc462ee871b9c647",
                "5291688c1ddd89b21ff8b610": {
                    "e65b9230480f23001af3196b": false,
                    "39f6fbebe12ae7734582252e": false,
                    "33359c6387984ebe487cac8e": true,
                    "bca87c93d879c6fce72fe24f": true,
                    "5f9064a1de91c47d8a0162f5": true
                },
                "312d03726673d3841c2565a5": "dddd",
                "efd03dfb18d8c50311741ebc": "7f02b4699bb3a6fb9305bd46",
                "c6c698a2fa543d8ac83bf335": {
                    "aeb637e4aebff88b3d2615c9": true,
                    "2528c80ed61c7d73f6c5dab1": true,
                    "8dac4e7d72292965a81551d1": false,
                    "d4b8cece80a2de58ddb53576": true,
                    "21e4cfe1fd4893b4aa7e1ad1": false
                },
                "06cbbd66c8310e57296b9f69": "dsa",
                "f44718c229f5eea3f0b11ab4": "7228a6ad032479f15e652af3",
                "60982d2736db214b9a0d87c8": {
                    "afd48455ed4e586a3b2bed89": false,
                    "4211c932d613fe50482d22ec": true,
                    "209e819c6a5b98e6fc3af6b4": false,
                    "029e0d4960392756ce4fa5f0": true,
                    "146466afe03a5b1d7e4a4d91": false
                },
                "53c462bb6dd45962c3c44c36": "dsa"
            },
            "sent": "2015-07-22T11:39:44.357Z"
        };
        var nee = {
            "reference_id": "3oCM6J9Nm8LNCnfHw",
            "answers": {
                "25516a5398a2be3d920cd360": "771f162153294d0688f83c64",
                "5291688c1ddd89b21ff8b610": {
                    "e65b9230480f23001af3196b": false,
                    "39f6fbebe12ae7734582252e": false,
                    "33359c6387984ebe487cac8e": true,
                    "bca87c93d879c6fce72fe24f": true,
                    "5f9064a1de91c47d8a0162f5": false
                },
                "312d03726673d3841c2565a5": "ddd",
                "efd03dfb18d8c50311741ebc": "7f02b4699bb3a6fb9305bd46",
                "c6c698a2fa543d8ac83bf335": {
                    "aeb637e4aebff88b3d2615c9": true,
                    "2528c80ed61c7d73f6c5dab1": true,
                    "8dac4e7d72292965a81551d1": false,
                    "d4b8cece80a2de58ddb53576": false,
                    "21e4cfe1fd4893b4aa7e1ad1": true
                },
                "06cbbd66c8310e57296b9f69": "dsadsa",
                "f44718c229f5eea3f0b11ab4": "7228a6ad032479f15e652af3",
                "60982d2736db214b9a0d87c8": {
                    "afd48455ed4e586a3b2bed89": true,
                    "4211c932d613fe50482d22ec": false,
                    "209e819c6a5b98e6fc3af6b4": false,
                    "029e0d4960392756ce4fa5f0": true,
                    "146466afe03a5b1d7e4a4d91": true
                },
                "53c462bb6dd45962c3c44c36": "dsadsa"
            },
            "sent": "2015-07-22T11:40:10.784Z"
        };
        var vii = {
            "reference_id": "3oCM6J9Nm8LNCnfHw",
            "answers": {
                "25516a5398a2be3d920cd360": "d9df26877bc503d47f4947a6",
                "5291688c1ddd89b21ff8b610": {
                    "e65b9230480f23001af3196b": false,
                    "39f6fbebe12ae7734582252e": true,
                    "33359c6387984ebe487cac8e": false,
                    "bca87c93d879c6fce72fe24f": true,
                    "5f9064a1de91c47d8a0162f5": false
                },
                "312d03726673d3841c2565a5": "edsa",
                "efd03dfb18d8c50311741ebc": "7f02b4699bb3a6fb9305bd46",
                "c6c698a2fa543d8ac83bf335": {
                    "aeb637e4aebff88b3d2615c9": false,
                    "2528c80ed61c7d73f6c5dab1": false,
                    "8dac4e7d72292965a81551d1": true,
                    "d4b8cece80a2de58ddb53576": true,
                    "21e4cfe1fd4893b4aa7e1ad1": true
                },
                "06cbbd66c8310e57296b9f69": "jcfktyugvgujn",
                "f44718c229f5eea3f0b11ab4": "633af5c97b9193afa6db9e82",
                "60982d2736db214b9a0d87c8": {
                    "afd48455ed4e586a3b2bed89": true,
                    "4211c932d613fe50482d22ec": true,
                    "209e819c6a5b98e6fc3af6b4": false,
                    "029e0d4960392756ce4fa5f0": false,
                    "146466afe03a5b1d7e4a4d91": false
                },
                "53c462bb6dd45962c3c44c36": "hluvu"
            },
            "sent": "2015-07-22T11:42:32.349Z"
        };
        var kuu = {
            "reference_id": "3oCM6J9Nm8LNCnfHw",
            "answers": {
                "25516a5398a2be3d920cd360": "771f162153294d0688f83c64",
                "5291688c1ddd89b21ff8b610": {
                    "e65b9230480f23001af3196b": false,
                    "39f6fbebe12ae7734582252e": false,
                    "33359c6387984ebe487cac8e": true,
                    "bca87c93d879c6fce72fe24f": true,
                    "5f9064a1de91c47d8a0162f5": false
                },
                "312d03726673d3841c2565a5": "ddd",
                "efd03dfb18d8c50311741ebc": "7f02b4699bb3a6fb9305bd46",
                "c6c698a2fa543d8ac83bf335": {
                    "aeb637e4aebff88b3d2615c9": true,
                    "2528c80ed61c7d73f6c5dab1": true,
                    "8dac4e7d72292965a81551d1": false,
                    "d4b8cece80a2de58ddb53576": false,
                    "21e4cfe1fd4893b4aa7e1ad1": true
                },
                "06cbbd66c8310e57296b9f69": "dsadsa",
                "f44718c229f5eea3f0b11ab4": "7228a6ad032479f15e652af3",
                "60982d2736db214b9a0d87c8": {
                    "afd48455ed4e586a3b2bed89": true,
                    "4211c932d613fe50482d22ec": false,
                    "209e819c6a5b98e6fc3af6b4": false,
                    "029e0d4960392756ce4fa5f0": true,
                    "146466afe03a5b1d7e4a4d91": true
                },
                "53c462bb6dd45962c3c44c36": "dsadsa"
            },
            "sent": "2015-07-22T11:40:10.784Z"
        };

        Inquiries.insert(test);
        Answers.insert(yyy);
        Answers.insert(kaa);
        Answers.insert(koo);
        Answers.insert(nee);
        Answers.insert(vii);
        Answers.insert(kuu);
    }



});