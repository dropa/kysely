/**
 * Created by dropa on 26.8.2015.
 */

Meteor.methods({
    insertAnswer: function(received){
        if(Roles.userIsInRole(Meteor.userId(),'reader')){
            console.log(received)
            received.push({user:Meteor.userId()});
            console.log(received);
            var id=Answers.insert(received);
            return id;
        }
        return false;
    },
    insertContact: function(received){
        if(Roles.userIsInRole(Meteor.userId(),'reader')){
            Contacts.insert(received);
            return true;
        }
        return false;
    },
    createNewInquiry: function (create) {
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            return Inquiries.insert(create);
        }
    },
    createNewAnswer: function (create){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Inquiries.update(
                {
                    questions:{
                        $elemMatch:{
                            "question_id": create.question_id
                        }
                    }},
                {
                    $set:{
                        "lastModifiedAt": create.lastModifiedAt,
                        "lastModifiedBy": create.lastModifiedBy,
                        "lastAction": create.lastAction
                    },
                    $push:{
                        "questions.$.options":{
                            "answer_id": create.answer_id,
                            "answerText": create.answerText
                        }
                    }
                }
            );
        }
    },
    createNewQuestionWithOptions: function(create){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Inquiries.update(
                {_id: create.id},
                {
                    $set:{
                        "lastModifiedAt": create.lastModifiedAt,
                        "lastModifiedBy": create.lastModifiedBy,
                        "lastAction": create.lastAction
                    },
                    $push:{
                        "questions":{
                            "question_id": create.question_id,
                            "questionType": create.questionType,
                            "questionText": create.questionText,
                            "options": create.options
                        }}}
            );
        }
    },
    createNewQuestionWithoutOptions: function(create){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Inquiries.update(
                {_id: create.id},
                {
                    $set:{
                        "lastModifiedAt": create.lastModifiedAt,
                        "lastModifiedBy": create.lastModifiedBy,
                        "lastAction": create.lastAction
                    },
                    $push:{
                        "questions":{
                            "question_id": create.question_id,
                            "questionType": create.questionType,
                            "questionText": create.questionText
                        }}}
            );
        }
    },
    deleteAnswer: function(remove){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Inquiries.update(
                {
                    "questions.options.answer_id": remove.answer_id
                },
                {
                    $set:{
                        "lastModifiedAt": remove.lastModifiedAt,
                        "lastModifiedBy": remove.lastModifiedBy,
                        "lastAction": remove.lastAction
                    },
                    $pull:{
                        "questions.$.options":{
                            "answer_id": remove.answer_id,
                            "text": remove.text
                        }
                    }
                }
            );
        }
    },
    deleteQuestion: function(remove){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Inquiries.update(
                {
                    "questions.question_id": remove.question_id
                },
                {
                    $set:{
                        "lastModifiedAt": remove.lastModifiedAt,
                        "lastModifiedBy": remove.lastModifiedBy,
                        "lastAction": remove.lastAction
                    },
                    $pull:{
                        "questions":{
                            question_id: remove.question_id,
                            questionText: remove.questionText
                        }
                    }
                }
            );
        }
    },
    updateInquiryTopic: function(update){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Inquiries.update(update.id,{
                $set:{
                    lastModifiedBy: update.lastModifiedBy,
                    lastModifiedAt: update.lastModifiedAt,
                    lastAction: update.lastAction,
                    topic:update.topic
                }
            });
        }
    },
    updateQuestionText: function(update){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Inquiries.update(
                {
                    questions:{
                        $elemMatch:{
                            "question_id": update.question_id
                        }
                    }
                },
                {
                    $set: {
                        lastModifiedBy: update.lastModifiedBy,
                        lastModifiedAt: update.lastModifiedAt,
                        lastAction: update.lastAction,
                        "questions.$.questionText": update.questionText
                    }
                }
            );
        }
    },
    updateAnswerText: function(update){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            var setModifier = { $set: {
                lastModifiedBy: update.lastModifiedBy,
                lastModifiedAt: update.lastModifiedAt,
                lastAction: update.lastAction
            } };
            setModifier.$set['questions.$.options.' + update.itemIndex + '.answerText'] = update.answerText;
            Inquiries.update(
                {
                    "questions.options":
                    {
                        $elemMatch:
                        {
                            "answer_id":
                                update.answer_id
                        }
                    }
                },
                setModifier
            );
        }
    },
    modifyActive: function(modify){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Inquiries.update(modify.id,
                {$set: {
                    lastAction: modify.lastAction,
                    lastModifiedAt: modify.lastModifiedAt,
                    lastModifiedBy: modify.lastModifiedBy,
                    active: ! modify.active
                }});
        }
    },
    deleteInquiry: function(id){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Inquiries.remove(id);
            Answers.remove({reference_id: id})
        }
    },
    pinImage: function(pin){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Inquiries.update(pin.parent,
                {$push: {
                    photos: pin.image}}
            );
        }
    },
    unPinImage: function(pin){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Inquiries.update(pin.parent,
                {$pull: {
                    photos: pin.image}}
            );
        }
    },
    deleteImage: function(image){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Images.remove(image);
            Inquiries.update({photos: image}, {$pull: {photos: image}}, {multi: true})

        }
    },
    addAdmin: function(id){
        if(Roles.userIsInRole(Meteor.userId(),'admin')){
            Roles.addUsersToRoles(id, 'admin');
        }
    },
    newUser: function(user){
        if(Roles.userIsInRole(Meteor.userId(),'admin')){
            Accounts.createUser(user);
        }
    },
    removeUser: function(user){
        if(Roles.userIsInRole(Meteor.userId(),'admin') && Meteor.userId() != user){
            Meteor.users.remove(user);
        }
    },
    resetUser: function(user){
        if(Roles.userIsInRole(Meteor.userId(),'admin') && Meteor.userId() != user.id){
            Accounts.setPassword(user.id,user.name,{logout: false});
        }
    },
    addMod: function(id){
        if(Roles.userIsInRole(Meteor.userId(),'admin')){
            Roles.addUsersToRoles(id, 'moderator');
        }
    },
    addRead: function(id){
        if(Roles.userIsInRole(Meteor.userId(),'admin')){
            Roles.addUsersToRoles(id, 'reader');
        }
    },
    removeAdmin: function(id){
        if(Roles.userIsInRole(Meteor.userId(),'admin')){
            Roles.removeUsersFromRoles(id, 'admin');
        }
    },
    removeMod: function(id){
        if(Roles.userIsInRole(Meteor.userId(),'admin')){
            Roles.removeUsersFromRoles(id, 'moderator');
        }
    },
    removeRead: function(id){
        if(Roles.userIsInRole(Meteor.userId(),'admin')){
            Roles.removeUsersFromRoles(id, 'reader');
        }
    }
});