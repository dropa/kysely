/**
 * Created by dropa on 26.8.2015.
 */
Inquiries = new Mongo.Collection("inquiries");
Answers = new Mongo.Collection("answers");
Contacts = new Mongo.Collection("contacts");
var imageStore = new FS.Store.GridFS("images");
Images = new FS.Collection("images", {
    stores: [imageStore],
    filter: {
        allow: {
            maxSize: 1048576, // in bytes
            contentTypes: ['image/*'] //allow only images in this FS.Collection
        },
        onInvalid: function (message) {
            if (Meteor.isClient) {
                alert(message);
            } else {
                console.log(message);
            }
        }
    }
});

Inquiries.defaultName = function() {
    var nextLetter = 'A', nextName = 'Kysely ' + nextLetter;
    while (Inquiries.findOne({topic: nextName})) {
        nextLetter = String.fromCharCode(nextLetter.charCodeAt(0) + 1);
        nextName = 'Kysely ' + nextLetter;
    }
    return nextName;
};

