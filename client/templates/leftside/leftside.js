/**
 * Created by dropa on 26.8.2015.
 */

Template.LeftSide.helpers({
    inquiries: function () {
        return Inquiries.find();
    },
    activeAmount: function () {
        return Inquiries.find({active:true}).count();
    },
    deactiveAmount: function() {
        return Inquiries.find({active:false}).count();
    }
});

Template.LeftSide.events({
    "click .renewPassword": function(){
        Session.set("alertOld", false);
        Session.set("alertNew", false);
        Session.set("alertConf", false);
        Modal.show('ChangePassword', this);
    },
    "click .selectInquiry": function() {
        Session.set("selectedInquiryId", this._id);
        Session.set("selectedPage", "inquiry");
    },
    'click .listUsers': function(){
        if(Roles.userIsInRole(Meteor.user(),'admin')){
            Session.set("selectedPage", "listUsers");
        }
    },
    'click .listContacts': function(){
        if(Roles.userIsInRole(Meteor.user(),'moderator')){
            Session.set("selectedPage", "listContacts");
        }
    },
    'click .imageBank': function(){
        if(Roles.userIsInRole(Meteor.user(),'moderator')){
            Session.set("selectedPage", "imageBank");
        }
    },
    'click .newInquiry': function() {
        var thisDate = new Date();
        var thisUser = Meteor.user().username;
        var create = {
            topic: Inquiries.defaultName(),
            createdAt: thisDate,
            createdBy: thisUser,
            lastAction: "Kysely luotu",
            active: false,
            "photos": [],
            lastModifiedAt: thisDate,
            lastModifiedBy: thisUser,
            questions: []
        };
        Meteor.call("createNewInquiry", create, function(error, result){
            Session.set("selectedInquiryId", result);
            Session.set("selectedPage", "inquiry");
        });
    },
    "click .landingPage": function(){
        Session.set("selectedInquiryId", false);
        Session.set("selectedPage", false);
    },
    "click .LogOut": function(){
        Meteor.logout();
    }
});