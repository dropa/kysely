/**
 * Created by dropa on 26.8.2015.
 */

Template.ListUsers.helpers({
    listUser: function(){
        if (Roles.userIsInRole(Meteor.user(), 'admin')) {
            return Meteor.users.find({}, {sort: {username: 1}});
        } else {
            // user not authorized. do not publish secrets
            return false;
        }
    },
    findAdmin: function(){
        return Roles.userIsInRole(this._id, 'admin');
    },
    findMod: function(){
        return Roles.userIsInRole(this._id, 'moderator');
    },
    findRead: function(){
        return Roles.userIsInRole(this._id, 'reader');
    }

});

Template.ListUsers.events({
    'click .newUser': function(){
        if(Roles.userIsInRole(Meteor.userId(), 'admin')){
            Modal.show("AddUser");
        }
        return false;
    },
    'click .addAdmin': function(){
        if(Roles.userIsInRole(Meteor.userId(), 'admin')){
            Meteor.call("addAdmin", this._id);
        }
        return false;
    },
    'click .addMod': function(){
        if(Roles.userIsInRole(Meteor.userId(), 'admin')){
            Meteor.call("addMod", this._id);
        }
        return false;
    },
    'click .addRead': function(){
        if(Roles.userIsInRole(Meteor.userId(), 'admin')){
            Meteor.call("addRead", this._id);
        }
        return false;
    },
    'click .removeAdmin': function(){
        if(Roles.userIsInRole(Meteor.userId(), 'admin')){
            if(Meteor.userId() == this._id){ alert("You can't remove your own rights!")}
            else {
                Meteor.call("removeAdmin", this._id);
            }
        }
        return false;
    },
    'click .removeMod': function(){
        if(Roles.userIsInRole(Meteor.userId(), 'admin')){
            if(Meteor.userId() == this._id){ alert("You can't remove your own rights!")}
            else {
                Meteor.call("removeMod", this._id);
            }
        }
        return false;
    },
    'click .removeRead': function(){
        if(Roles.userIsInRole(Meteor.userId(), 'admin')){
            if(Meteor.userId() == this._id){ alert("You can't remove your own rights!")}
            else{
                Meteor.call("removeRead", this._id);
            }
        }
        return false;
    },
    'click .removeUser': function(){
        if(Meteor.userId() == this._id){
            alert("Nope.");
        }
        else{
            Modal.show("DelUser",this);
        }
        return false;
    },
    'click .rstUser': function(){
        if(Meteor.userId() == this._id){
            alert("Nope.");
        }
        else{
            Modal.show("RstUser",this);
        }
        return false;
    }
});