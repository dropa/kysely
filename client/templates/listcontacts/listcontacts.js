/**
 * Created by dropa on 26.8.2015.
 */

Template.ListContacts.helpers({
    contacts: function(){
        if (Roles.userIsInRole(Meteor.user(), 'moderator')){
            return Contacts.find();
        }
    },
    birthdayFormat: function(){
        if(this.birthday) {
            return moment(this.birthday).format('DD.MM.YYYY');
        }
        else return false;
    }
});

Template.ListContacts.events({
    'click .downloadCsv': function() {
    var date = new Date(),
        formedDate = moment(date).format('DD.MM.YYYY'),
        formedTime = moment(date).format('HH:mm:ss'),
        extension = ".csv",
        file = "kontaktit",
        filename = formedDate + file + extension,
        totalAmount = Contacts.find().count(),
        separator = "\t",
        breaker = "\n",
        fileContent = "sep=" + separator + breaker;

    filename = filename.replace(/ /g, '');

    fileContent += "Kontaktit" + breaker;
    fileContent += "Kpl" + separator + totalAmount + breaker;
    fileContent += "Pvm" + separator + formedDate + breaker;
    fileContent += "Klo" + separator + formedTime + breaker;
    fileContent += breaker;
    fileContent += "Vastaanotettu" + separator +
        "Sukunimi" + separator +
        "Etunimi" + separator +
        "Syntynyt" + separator +
        "Puhelin" + separator +
        "Email" + separator +
        "Sukupuoli" + separator +
        "Osoite" + separator +
        "Postinro" + separator +
        "Kaupunki" + breaker;

    var contacts = Contacts.find().fetch();

    for(var i = 0; contacts[i]; i++){
        fileContent += moment(contacts[i].sent).format('DD.MM.YYY HH:mm:ss') || "";
        fileContent += separator;
        fileContent += contacts[i].surname || "";
        fileContent += separator;
        fileContent += contacts[i].firstname || "";
        fileContent += separator;
        if(contacts[i].birthday){
            fileContent += moment(contacts[i].birthday).format('DD.MM.YYYY');
        }
        fileContent += separator;
        fileContent += contacts[i].phone || "";
        fileContent += separator;
        fileContent += contacts[i].email || "";
        fileContent += separator;
        fileContent += contacts[i].gender || "";
        fileContent += separator;
        fileContent += contacts[i].address || "";
        fileContent += separator;
        fileContent += contacts[i].zip || "";
        fileContent += separator;
        fileContent += contacts[i].city || "";
        fileContent += separator + breaker;
    }

    var blob = new Blob([fileContent], {type: "text/plain; charset=utf-8"});
    saveAs(blob, filename);
}
});