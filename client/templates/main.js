/**
 * Created by dropa on 26.8.2015.
 */


Meteor.subscribe("inquiries");
Meteor.subscribe("images");

Template.registerHelper('formatDate', function(date) {
        return moment(date).format('DD.MM.YYYY HH:mm:ss');
    }
);