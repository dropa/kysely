/**
 * Created by dropa on 26.8.2015.
 */

Template.InquiryTools.events({
    "click .modifyActive": function(){
        var action;
        if (!this.active){
            action = "Otettu k" +
                String.fromCharCode(228) +
                "ytt" +
                String.fromCharCode(246,246) +
                "n";
        } else {
            action = "Poistettu k" +
                String.fromCharCode(228) +
                "yt" +
                String.fromCharCode(246) +
                "st" +
                String.fromCharCode(228);
        }
        var modify = {
            id: this._id,
            active: this.active,
            lastAction: action,
            lastModifiedBy: Meteor.user().username,
            lastModifiedAt: new Date()
        };
        Meteor.call("modifyActive", modify);
    },
    "click .deleteInquiry": function(){
        this.deleteObjectType = "deleteInquiry";
        this.deleteObjectName = "kyselyn";
        this.deleteObjectValue = this.topic;
        Modal.show('DeletePopup', this);
    }
});