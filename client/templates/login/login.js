/**
 * Created by dropa on 26.8.2015.
 */

Template.Login.events({
    'submit #login': function(event, template) {
        // 1. Collect the username and password from the form
        var username = template.find('#login-username').value,
            password = template.find('#login-password').value;

        // 2. Attempt to login.
        Meteor.loginWithPassword(username, password, function(error) {
            // 3. Handle the response
            if (!Meteor.userId()){
                // If no user resulted from the attempt, an error variable will be available
                // in this callback. We can output the error to the user here.
                var message = "There was an error logging in: " + error.reason;

                alert(message);
            }
        });
        return false;
    }
});