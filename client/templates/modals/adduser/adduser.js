/**
 * Created by dropa on 26.8.2015.
 */

Template.AddUser.events({
    'submit .newUser': function(event, template){
        if(Roles.userIsInRole(Meteor.userId(), 'admin')){
            var fetched = template.find('#username').value;
            var user = {
                username: fetched,
                password: fetched
            };
            Meteor.call("newUser", user);
        }
        Modal.hide("AddUser");
        return false;
    }
});
