/**
 * Created by dropa on 26.8.2015.
 */

Template.DelUser.events({
    'click .confirmed': function(){
        Meteor.call("removeUser", this._id);
        Modal.hide("DelUser");
        return false;
    }
});
