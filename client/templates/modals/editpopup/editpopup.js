/**
 * Created by dropa on 26.8.2015.
 */

Template.EditPopup.events({
    "submit .editableObject": function(){
        var editAction, update = "";
        if(this.objectToEdit == "inquiryTopic") {
            update = {
                id: this._id,
                lastModifiedBy: Meteor.user().username,
                lastModifiedAt: new Date(),
                lastAction: "Kysely uudelleennimetty",
                topic: event.target.text.value || "<Please rename this object>"
            };
            editAction = "updateInquiryTopic";
        }
        else if (this.objectToEdit == "questionText"){
            update = {
                question_id: this.question_id,
                lastModifiedBy: Meteor.user().username,
                lastModifiedAt: new Date(),
                lastAction: "Kysymys uudelleennimetty",
                questionText: event.target.text.value || "<Please rename this object>"
            };
            editAction = "updateQuestionText";
        }
        else if (this.objectToEdit == "answerText"){
            update = {
                itemIndex: this.index,
                answer_id: this.answer_id,
                lastModifiedBy: Meteor.user().username,
                lastModifiedAt: new Date(),
                lastAction: "Vastausvaihtoehto uudelleennimetty",
                answerText: event.target.text.value || "<Please rename this object>"
            };
            editAction = "updateAnswerText";
        }
        Meteor.call(editAction, update);
        Modal.hide('EditPopup');
        return false;
    }
});
