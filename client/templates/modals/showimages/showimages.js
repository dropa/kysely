/**
 * Created by dropa on 26.8.2015.
 */

Template.ShowImages.helpers({
    'images': function(){
        return Images.find();
    },
    'isSelected': function(){
        var image = this._id;
        var check = Inquiries.findOne(Template.parentData()._id);
        if(check.photos.indexOf(image) !== -1){
            return true;
        }
        return false;
    }
});

Template.ShowImages.events({
    'click .pin': function(){
        var pin = {
            parent: Session.get("selectedInquiryId"),
            image: this._id
        };
        Meteor.call("pinImage", pin);
    },
    'click .unPin': function(){
        var pin = {
            parent: Session.get("selectedInquiryId"),
            image: this._id
        };
        Meteor.call("unPinImage", pin);
    }
});