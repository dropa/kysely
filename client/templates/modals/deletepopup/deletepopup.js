/**
 * Created by dropa on 26.8.2015.
 */

Template.DeletePopup.events({
    "submit .deleteInquiry": function(){
        Meteor.call("deleteInquiry", this._id);
        Session.set("selectedPage", false);
        Session.set("selectedInquiryId", false);
        Modal.hide('DeletePopup');
        return false;
    },
    "submit .deleteQuestion": function(){
        var remove = {
            lastModifiedBy: Meteor.user().username,
            lastModifiedAt: new Date(),
            lastAction: "Kysymys poistettu",
            question_id: this.question_id,
            questionText: this.questionText
        };
        Meteor.call("deleteQuestion", remove);
        Modal.hide('DeletePopup');
        return false;
    }
});
