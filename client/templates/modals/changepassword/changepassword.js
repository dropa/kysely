/**
 * Created by dropa on 26.8.2015.
 */

Template.ChangePassword.helpers({
    alertOld:function(){
        return Session.get("alertOld") || false;
    },
    alertNew:function(){
        return Session.get("alertNew") || false;
    },
    alertConf:function(){
        return Session.get("alertConf") || false;
    }
});

Template.ChangePassword.events({
    "submit .changePassword": function(event, template){
        var Pold = template.find('#oldPass').value,
            Pnew = template.find('#newPass').value,
            Pcon = template.find('#conPass').value;
        if(Pnew.length >= 6){
            Session.set("alertNew", false);
            if(Pcon === Pnew){
                Session.set("alertConf", false);
                Accounts.changePassword(Pold,Pnew,function(error){
                    if(error) {
                        Session.set("alertOld",true);
                    }
                    else{
                        Session.set("alertOld", false);
                        Modal.hide('ChangePassword');
                    }
                });
            }
            else {
                Session.set("alertConf",true);
            }
        }
        else {
            Session.set("alertNew", true);
        }
        return false;
    }
});
