/**
 * Created by dropa on 26.8.2015.
 */

Template.RstUser.events({
    'click .confirmed': function(){
        var user = {
            id: this._id,
            name: this.username
        };
        Meteor.call("resetUser", user);
        Modal.hide("RstUser");
        return false;
    }
});
