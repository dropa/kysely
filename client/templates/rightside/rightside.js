/**
 * Created by dropa on 26.8.2015.
 */

Template.RightSide.helpers({
    selectPage: function(){
        var ID = Session.get("selectedInquiryId");
        var page = Session.get("selectedPage");
        if(ID && page == "inquiry"){
            return {template: 'ListItem', data: Inquiries.findOne(ID)};
        }
        else if(page == "listUsers"){
            return {template: 'ListUsers'};
        }
        else if(page == 'listContacts'){
            return {template: 'ListContacts'};
        }
        else if(page == 'imageBank'){
            return {template: 'ImageBank'};
        }
        else {
            return {template: 'LandingPage'};
        }
    }
});

Template.RightSide.events({
    "click .editInquiry": function(){
        this.objectTopic = "Kysely";
        this.objectToEdit = "inquiryTopic";
        this.objectValue = this.topic;
        Modal.show('EditPopup', this);
    },
    "click .editQuestionText": function(){
        this.objectTopic = "Kysymys";
        this.objectToEdit = "questionText";
        this.objectValue = this.questionText;
        Modal.show('EditPopup', this);
    },
    "click .editvastaus": function(){
        this.objectTopic = "Vastausvaihtoehto";
        this.objectToEdit = "answerText";
        this.objectValue = this.answerText;
        Modal.show('EditPopup', this);
    }
});