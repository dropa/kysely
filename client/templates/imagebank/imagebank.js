/**
 * Created by dropa on 26.8.2015.
 */
Template.ImageBank.helpers({
    'images': function(){
        return Images.find();
    }
});
Template.ImageBank.events({
    'change .imageUpload': function(event,template){
        event.preventDefault();
        FS.Utility.eachFile(event, function(file) {
            Images.insert(file,function(err,fileObj){
                if (err){
                    alert("Upload failed. If this error happens again, please report it.")
                    console.log(fileObj);
                    console.log(err.message);
                }
            })
        })
    },
    'click .removeImage': function(){
        if(Roles.userIsInRole(Meteor.userId(),'moderator')){
            Meteor.call("deleteImage",this._id);
        }
    }
});
