/**
 * Created by dropa on 26.8.2015.
 */
Template.ListItem.helpers({
    options: function(){
        var self = this;
        self.options = self.options || [];
        return _.map(self.options, function(value, index){
            var totalAmount = Answers.find({reference_id: Template.parentData()._id}).count();
            return {
                question_id: self.question_id,
                questionType: self.questionType,
                answerText: value.answerText,
                answer_id:value.answer_id,
                index: index,
                totalAmount: totalAmount
            };
        });
    },
    totalAnswersAmount: function(){
        return Answers.find({reference_id: this._id}).count();
    },
    thisQuestionType: function(){
        var type = "";
        if(this.questionType == "radio"){
            type = "glyphicon-record";
        }
        else if (this.questionType == "checkbox"){
            type = "glyphicon-check";
        }
        else if (this.questionType == "input"){
            type = "glyphicon-log-in";
        }
        return type;
    },
    notInputType: function(){
        return this.questionType != "input";
        /* if(this.questionType == "input"){
         return false;
         }
         else{
         return true;
         }*/
    }
});

Template.ListItem.events({
    'click .newAnswer': function() {
        var create = {
            lastModifiedAt: new Date(),
            lastModifiedBy: Meteor.user().username,
            lastAction: "Uusi vastausvaihtoehto",
            question_id: this.question_id,
            answer_id: new Meteor.Collection.ObjectID()._str,
            answerText: "Uusi vastaus"
        };
        Meteor.call("createNewAnswer", create);
    },
    'click .newQuestionRadio': function() {
        var create = {
            lastModifiedAt: new Date(),
            lastModifiedBy: Meteor.user().username,
            lastAction: "Uusi kysymys (yksivalinta)",
            id: this._id,
            question_id: new Meteor.Collection.ObjectID()._str,
            questionType: "radio",
            questionText: "Uusi kysymys",
            options: []
        };
        Meteor.call("createNewQuestionWithOptions", create);
    },
    'click .newQuestionCheck': function() {
        var create = {
            lastModifiedAt: new Date(),
            lastModifiedBy: Meteor.user().username,
            lastAction: "Uusi kysymys (monivalinta)",
            id: this._id,
            question_id: new Meteor.Collection.ObjectID()._str,
            questionType: "checkbox",
            questionText: "Uusi kysymys",
            options: []
        };
        Meteor.call("createNewQuestionWithOptions", create);
    },
    'click .newQuestionInput': function() {
        var create = {
            lastModifiedAt: new Date(),
            lastModifiedBy: Meteor.user().username,
            lastAction: "Uusi kysymys (vapaavastaus)",
            id: this._id,
            question_id: new Meteor.Collection.ObjectID()._str,
            questionType: "input",
            questionText: "Uusi kysymys"
        };
        Meteor.call("createNewQuestionWithoutOptions", create);
    },
    'click .removeQuestion': function(){
        this.deleteObjectType = "deleteQuestion";
        this.deleteObjectName = "kysymyksen";
        this.deleteObjectValue = this.questionText;
        Modal.show('DeletePopup', this);
    },
    'click .removeAnswer': function(){
        var remove = {
            lastModifiedBy: Meteor.user().username,
            lastModifiedAt: new Date(),
            lastAction: "Vastausvaihtoehto poistettu",
            answerText: this.answerText,
            answer_id: this.answer_id
        };
        Meteor.call("deleteAnswer", remove);
    },
    'click .showImages': function() {
        if (Roles.userIsInRole(Meteor.userId(), 'moderator')) {
            Modal.show("ShowImages", this);
        }
        return false;
    },
    'click .downloadCsv': function() {
        var date = new Date(),
            formedDate = moment(date).format('DD.MM.YYYY'),
            formedTime = moment(date).format('HH:mm:ss'),
            extension = ".csv",
            file = this.topic,
            filename = formedDate + file + extension,
            totalAmount = Answers.find({reference_id: this._id}).count(),
            separator = "\t",
            breaker = "\n",
            fileContent = "sep=" + separator + breaker;


        filename = filename.replace(/ /g, '');

        fileContent += "Kysely" + separator + this.topic + breaker;
        fileContent += "Vastauskerrat" + separator + totalAmount + breaker;
        fileContent += "Pvm" + separator + formedDate + breaker;
        fileContent += "Klo" + separator + formedTime + breaker;
        fileContent += breaker;
        for (var i = 0; this.questions[i]; i++) {

             var searchParameters = {};
             fileContent += this.questions[i].questionText + breaker;
             if(this.questions[i].questionType !== 'input'){
             fileContent += "Kysymys" + separator + "kpl" + separator + "Osuus" + breaker;
             for(var j = 0; this.questions[i].options[j]; j++){
             searchParameters = {reference_id: this._id};
             var answerCount,
             answerPercentage;

             if (this.questions[i].questionType == "radio") {
             searchParameters["answers." + this.questions[i].question_id] = this.questions[i].options[j].answer_id;
             answerCount = Answers.find(searchParameters).count();

             }
             else if (this.questions[i].questionType == "checkbox") {
             searchParameters["answers." + this.questions[i].question_id + "." + this.questions[i].options[j].answer_id] = true;
             answerCount = Answers.find(searchParameters).count();
             }
             answerPercentage = answerCount / totalAmount * 100;
             fileContent += this.questions[i].options[j].answerText + separator;
             fileContent += answerCount + separator + parseFloat(answerPercentage).toFixed(2) + "%";

             fileContent += breaker;
             }
             }
             else if(this.questions[i].questionType === 'input'){
             searchParameters = {reference_id: this._id};
             var findValue = {},
             object = this.questions[i].question_id,
             fetched = Answers.find(searchParameters,{fields:findValue}).fetch();
             findValue["answers." + object] = 1;


             fileContent += separator + "Vastaukset" + breaker;
             for(var j = 0; fetched[j]; j++){
             fileContent += separator + fetched[j].answers[object] + breaker;
             }
             }
             fileContent += breaker;

             }

             var blob = new Blob([fileContent], {type: "text/plain; charset=utf-8"});
             saveAs(blob, filename);

    }
});
